# A simple proxy enabling to access websockets using HTTP requests.

## Usage

### Launch the proxy

```bash
java -jar ws2http-1.0.jar -b 0.0.0.0 -p 5368
```

Arguments:
- -b / --bind - [OPTIONAL, default: 0.0.0.0] which TCP/IP address to bing the HTTP Server to
- -p / --port - [OPTIONAL, default: 5368] which TCP/IP port should the HTTP Server be listening on

### Open a Proxy→WS connection

```bash
curl -sLk 'http://localhost:5368/connect?url=ws://localhost:10000/&shared=true&queue=10'
```

Opens the Proxy→WS connection and stores it in the Proxy. Use further calls to interact with the connection.

Parameters:
- url - [MANDATORY] the URL to connect to
- shared - [OPTIONAL, default: false] whether this is a shared connection, i.e. no connection ID is required to communicate with it
- queue - [OPTIONAL, default: 0] how many messages received via the WS while noone was reading them should the proxy cache internally. If 0, the cache is disabled and only messages received while /poll is active will be propagated.

Returns a connection ID. Use the connectionID for subsequent calls either as a "?token=${id}" query param or as an "Authorization: Bearer ${id}" header to identify which WS connection are you aiming for (`token` takes precedence)

### List established Proxy→WS connections

```bash
curl -siLk "http://localhost:5368/list?url=ws://.*"
```

Returns a 'id=url' newline-separated list of currently available (established) Proxy→WS connections 

Parameters:

- url - [OPTIONAL, default: .*] a regex pattern to filter the connections' URLs by

### Send data to the socket

```bash
1. curl -siLk "http://localhost:5368/send?url=ws://localhost:10000/" -d 'hello555'

2. curl -siLk "http://localhost:5368/send?token=aaaa-bbbb-cccc-dddd-eeee" -d 'hello555'
3. curl -siLk "http://localhost:5368/send" -H "Authorization: Bearer aaaa-bbbb-cccc-dddd-eeee" -d 'hello555'
```

'1' sends a message to a shared socket. To do so no unique identifier is required, just specify the URL you're referring to

'2' and '3' are equivalent and send a message to a speciffic WS connection. There can be any number of active connections at any given time.

Parameters:

- url - [OPTIONAL, but either `url` or `token` is REQUIRED]. The WS connection identifier


### Poll data from the socket

```bash
curl -siLk "http://localhost:5368/poll?url=ws://localhost:10000/&timeout=2&count=5&delim=@"
```

Polls the Proxy→WS connection waiting for messages to be received. If `count` is specified, only when all the `count` messages are received (or `timeout` is reached) the response is complete.

Parameters:

- url - [OPTIONAL, but either `url` or `token` is REQUIRED]. The WS connection identifier
- timeout - [OPTIONAL, default: 30] how long to wait for the data
- count - [OPTIONAL, default: 1] how many messages to be waiting for before closing the request
- delim - [OPTIONAL, default: \n] how each message received through the WS connection will be separated in the HTTP response body

### Close the WS connection

```bash
curl -siLk "http://localhost:5368/close?url=ws://localhost:10000/"
```

Closes the Proxy→WS connection

Parameters:

- url - [OPTIONAL, but either `url` or `token` is REQUIRED]. The WS connection identifier


