package com.netikras.tools.wshttp;

import lombok.Getter;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.java_websocket.util.NamedThreadFactory;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;

@Getter
public class WebSocketService {

    private final ExecutorService executorService;
    private final Map<String, WSConn> connections;

    public WebSocketService() {
        executorService = Executors.newFixedThreadPool(20, new NamedThreadFactory("ws-conn-thr-"));
        connections = new ConcurrentHashMap<>();
    }

    public Optional<WSConn> find(String id) {
        return Optional.ofNullable(connections.get(id));
    }

    public Future<WSConn> connect(URI uri, Map<String, String> headers, int timeoutSeconds) {
        return connect(uri, headers, timeoutSeconds, 0, false);
    }

    public Future<WSConn> connect(URI uri, Map<String, String> headers, int timeoutSeconds, int queueSize, boolean shared)
            throws IllegalStateException {
        CompletableFuture<WSConn> promise = new CompletableFuture<>();
        WSConn conn;
        if (shared) {
            String sharedId = getSharedId(uri.toString());
            if (find(sharedId).isPresent()) {
                throw new IllegalStateException("Cannot open another shared connection to " + uri + ". One already exists");
            }
            conn = new WSConn(sharedId, uri.toString(), queueSize);
        } else {
            conn = new WSConn(uri.toString());
        }

        WebSocketClient client = new WebSocketClient(uri, headers) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                conn.open();
                promise.complete(conn);
            }

            @Override public void onMessage(String message) {
                conn.recvMessage(message);
            }

            @Override public void onMessage(ByteBuffer bytes) {
                conn.recvMessage(bytes.array());
            }

            @Override public void onClose(int code, String reason, boolean remote) {
                conn.recvClose(code, reason, remote);
                connections.remove(conn.getId());
            }

            @Override public void onError(Exception ex) {
                conn.recvError(ex);
            }
        };
        conn.setWs(client);

        executorService.submit(() -> {
            try {
                if (client.connectBlocking(timeoutSeconds, TimeUnit.SECONDS)) {
                    conn.subscribeClose(client::close, null);
                    conn.subscribeClose((c, r) -> connections.remove(conn.getId()), (c, r) -> !connections.containsKey(conn.getId()));
                    connections.put(conn.getId(), conn);
                } else {
                    throw new IllegalStateException("Could not connect to " + uri + " in " + timeoutSeconds + " seconds");
                }
            } catch (Exception e) {
                promise.completeExceptionally(e);
            }
        });
        return promise;
    }

    protected String getSharedId(String uri) {
        String sharedId = "SHARED_" + uri;
        return sharedId;
    }

    public Optional<WSConn> getShared(String url) {
        return find(getSharedId(url));
    }
}
