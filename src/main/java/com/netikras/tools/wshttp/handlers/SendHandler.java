package com.netikras.tools.wshttp.handlers;

import com.netikras.tools.wshttp.WSConn;
import com.netikras.tools.wshttp.WebSocketService;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
public class SendHandler extends GenericServlet {
    private final WebSocketService webSocketService;

    public SendHandler(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
    }

    @Override
    protected void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        byte[] body = readAll(req.getInputStream());
        String connId = Optional.ofNullable(req.getParameter("token"))
                .orElseGet(() -> Optional.ofNullable(req.getHeader("Authorization"))
                        .filter(token -> token.toLowerCase().startsWith("bearer "))
                        .map(token -> token.substring("Bearer ".length()))
                        .orElse(null));

        WSConn wsConn = findConnection(connId, req.getParameter("url"), webSocketService);
        try {
            wsConn.send(body);
            send(resp, 200);
        } catch (Exception e) {
            log.error("Cannot send to " + connId, e);
            send(resp, 500, "Cannot send to " + connId + ". Error: " + e.getMessage());
        }
    }
}
