package com.netikras.tools.wshttp.handlers;

import com.netikras.tools.wshttp.WSConn;
import com.netikras.tools.wshttp.WebSocketService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
public class CloseHandler extends GenericServlet {
    private final WebSocketService webSocketService;

    public CloseHandler(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
    }

    @Override
    protected void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String connId = Optional.ofNullable(req.getParameter("token"))
                .orElseGet(() -> Optional.ofNullable(req.getHeader("Authorization"))
                        .filter(token -> token.toLowerCase().startsWith("bearer "))
                        .map(token -> token.substring("Bearer ".length()))
                        .orElse(null));

        int code = Optional.ofNullable(req.getParameter("code"))
                .map(Integer::parseInt)
                .orElse(-1);
        String reason = Optional.ofNullable(req.getParameter("reason"))
                .orElse("UNKNOWN");

        WSConn wsConn = findConnection(connId, req.getParameter("url"), webSocketService);

        try {
            wsConn.close(code, reason);
            send(resp, 200);
        } catch (Exception e) {
            log.error("Cannot close " + connId, e);
            send(resp, 500, "Cannot close " + connId + ". Error: " + e.getMessage());
        }

    }
}
