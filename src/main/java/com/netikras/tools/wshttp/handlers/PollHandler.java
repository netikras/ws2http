package com.netikras.tools.wshttp.handlers;

import com.netikras.tools.wshttp.WSConn;
import com.netikras.tools.wshttp.WebSocketService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

@Slf4j
public class PollHandler extends GenericServlet {

    private final WebSocketService webSocketService;
    private final String defaultDelimiter;

    public PollHandler(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
        defaultDelimiter = "\n";
    }

    @Override
    protected void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String connId = Optional.ofNullable(req.getParameter("token"))
                .orElseGet(() -> Optional.ofNullable(req.getHeader("Authorization"))
                        .filter(token -> token.toLowerCase().startsWith("bearer "))
                        .map(token -> token.substring("Bearer ".length()))
                        .orElse(null));

        int timeout = Optional.ofNullable(req.getParameter("timeout")).map(Integer::parseInt).orElse(30);
        int msgCount = Optional.ofNullable(req.getParameter("count")).map(Integer::parseInt).orElse(1);
        String separator = Optional.ofNullable(req.getParameter("delim")).orElse(defaultDelimiter);

        WSConn conn = findConnection(connId, req.getParameter("url"), webSocketService);

        List<byte[]> buffer = new ArrayList<>(msgCount);
        CompletableFuture<List<byte[]>> poller = new CompletableFuture<>();
        Map<String, String> subscriptions = new ConcurrentHashMap<>(3);

        subscriptions.put("MESSAGE", conn.subscribeMessage(msg -> {
            try {
                if (!buffer.isEmpty()) {
                    write(resp, 200, separator.getBytes());
                }
                write(resp, 200, msg);

                buffer.add(msg);
                if (buffer.size() >= msgCount) {
                    poller.complete(buffer);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, msg -> poller.isDone()));
        subscriptions.put("ERROR", conn.subscribeErrors(poller::completeExceptionally, msg -> poller.isDone()));
        subscriptions.put("CLOSE", conn.subscribeClose(
                (code, reason) -> poller.completeExceptionally(new IOException("Closed. code: " + code + ", reason: [" + reason + "]")),
                (code, reason) -> poller.isDone()));
        try {
            List<byte[]> messages = poller.get(timeout, TimeUnit.SECONDS);
            //            send(resp, 200, message);
            close(resp);
        } catch (TimeoutException e) {
            send(resp, 204);
            poller.completeExceptionally(e);
        } catch (Exception e) {
            log.error("Cannot read from " + connId, e);
            send(resp, 500, "Cannot read from " + connId + ". Error: " + e.getMessage());
        } finally {
            subscriptions.forEach((type, sub) -> conn.unsubscribe(sub));
        }
    }
}
