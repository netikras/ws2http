package com.netikras.tools.wshttp.handlers;

import com.netikras.tools.wshttp.WebSocketService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

@Slf4j
public class ListConnectionsHandler extends GenericServlet {
    private final WebSocketService webSocketService;

    public ListConnectionsHandler(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
    }

    @Override
    protected void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String urlPattern = Optional.ofNullable(req.getParameter("url")).orElse(".*");

        try {
            String result = webSocketService.getConnections().entrySet().stream()
                    .filter(e -> e.getValue().getUrl().matches(urlPattern))
                    .map(e -> String.format("%s=%s", e.getKey(), e.getValue().getUrl()))
                    .collect(Collectors.joining("\n"));

            if (result.isBlank()) {
                send(resp, 204);
            } else {
                send(resp, 200, result);
            }
        } catch (PatternSyntaxException e) {
            log.error("Bad URL pattern " + urlPattern, e);
            send(resp, 400, "Bad URL pattern: /" + urlPattern + "/. Error: " + e.getMessage());
        }catch (Exception e) {
            log.error("Cannot list connections matching " + urlPattern, e);
            send(resp, 500, "Cannot list connections matching /" + urlPattern + "/. Error: " + e.getMessage());
        }

    }
}
