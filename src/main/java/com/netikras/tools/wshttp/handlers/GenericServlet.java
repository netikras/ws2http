package com.netikras.tools.wshttp.handlers;

import com.netikras.tools.wshttp.WSConn;
import com.netikras.tools.wshttp.WebSocketService;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public abstract class GenericServlet extends HttpServlet {

    public static final HttpServlet NOT_FOUND = new GenericServlet() {
        @Override protected void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
            send(response, 404);
        }
    };

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String reqId = UUID.randomUUID().toString();
        try {
            log.info("Got request {} {}:{} {}", reqId, req.getMethod(), req.getRequestURI(), getClass().getSimpleName());
            handle(req, resp);
        } catch (BadRequestException e) {
            resp.setStatus(e.getCode());
            if (e.getMessage() != null) {
                resp.getOutputStream().write(e.getMessage().getBytes());
            }
            resp.flushBuffer();
            log.error("Request processing error", e);
            send(resp, 500, e.getMessage());
        } catch (Exception e) {
            log.error("Request processing error", e);
            send(resp, 500, e.getMessage());
        } finally {
            log.info("Response sent for request {}: {}", reqId, resp.getStatus());
        }
    }

    protected abstract void handle(HttpServletRequest request, HttpServletResponse response) throws Exception;

    protected void send(HttpServletResponse resp, int code, String payload) throws IOException {
        send(resp, code, payload == null ? null : payload.getBytes());
    }

    protected void write(HttpServletResponse resp, int code, byte[] data) throws IOException {
        resp.setStatus(code);
        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.write(data);
        outputStream.flush();
    }

    protected void close(HttpServletResponse resp) throws IOException {
        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.flush();
        resp.flushBuffer();
        outputStream.close();
    }

    protected void send(HttpServletResponse resp, int code, byte[] payload) throws IOException {
        resp.setStatus(code);
        ServletOutputStream outputStream = resp.getOutputStream();
        if (payload != null && payload.length > 0) {
            outputStream.write(payload);
        }
        close(resp);
    }

    protected void send(HttpServletResponse resp, int code) throws IOException {
        send(resp, code, (byte[]) null);
    }

    protected byte[] readAll(InputStream is) throws IOException {
        int nRead;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[16384];

        while ((nRead = is.read(buffer, 0, buffer.length)) != -1) {
            baos.write(buffer, 0, nRead);
        }
        byte[] data = baos.toByteArray();
        baos.close();
        return data;
    }

    protected WSConn findConnection(String connId, String url, WebSocketService webSocketService) throws BadRequestException {
        Optional<WSConn> wsConn;
        if (connId == null || connId.trim().isEmpty()) {
            if (url == null || url.isBlank()) {
                throw new BadRequestException(400, "Neither `token` or Bearer (connection ID) nor `url` specified (for shared connection)");
            } else {
                wsConn = webSocketService.getShared(url);
                if (wsConn.isEmpty()) {
                    throw new BadRequestException(400, "Cannot find shared connection to " + url);
                }
            }
        } else {
            wsConn = webSocketService.find(connId);
            if (wsConn.isEmpty()) {
                throw new BadRequestException(400, "Cannot find connection '" + connId + "'");
            }
        }
        return wsConn.get();
    }


    @Getter
    public static class BadRequestException extends Exception {
        private final int code;

        public BadRequestException(int code, String message) {
            super(message);
            this.code = code;
        }
    }
}
