package com.netikras.tools.wshttp.handlers;

import com.netikras.tools.wshttp.WSConn;
import com.netikras.tools.wshttp.WebSocketService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.function.Function;

@Slf4j
public class ConnectHandler extends GenericServlet {
    private final WebSocketService webSocketService;

    public ConnectHandler(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
    }

    @Override
    protected void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String url = req.getParameter("url");

        int timeout = Optional.ofNullable(req.getParameter("timeout"))
                .map(Integer::parseInt)
                .orElse(10);

        int queueSize = Optional.ofNullable(req.getParameter("queue")).map(Integer::parseInt).orElse(0);

        boolean shared = Optional.ofNullable(req.getParameter("shared")).map(Boolean::parseBoolean).orElse(false);

        Future<WSConn> connPromise = webSocketService.connect(URI.create(url), new HashMap<>(0), timeout, queueSize, shared);
        try {
            WSConn conn = connPromise.get();
            send(resp, 201, conn.getId());
        } catch (Exception e) {
            log.error("Cannot connect to " + url, e);
            send(resp, 500, "Cannot connect to " + url + ". Error: " + e.getMessage());
        }
    }

}
