package com.netikras.tools.wshttp;

import com.netikras.tools.wshttp.handlers.*;
import jakarta.servlet.Servlet;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class App {

    private final String contextPath = "";
    private final WebSocketService webSocketService;

    public App() {
        webSocketService = new WebSocketService();
    }

    public static void main(String[] args) throws Exception {
        String serverIp = findArg(Set.of("-b","--bind"), args).orElse("0.0.0.0");
        int serverPort = findIntArg(Set.of("-p", "--port"), args).orElse(5368);
        App app = new App();
        log.info("Starting server at {}:{}", serverIp, serverPort);
        Server server = app.startServer(serverIp, serverPort);
        log.info("Server started");
//        server.dumpStdErr();
        server.join();
    }

    private static Optional<String> findArg(Set<String> argNames, String... args) {
        String last = null;
        for (int i = 0; i < args.length; i++) {
            if (argNames.contains(args[i])) {
                if (i + 1 < args.length) {
                    last = args[i+1];
                } else {
                    throw new IllegalArgumentException("Cannot find value for the last arg " + argNames);
                }
            }
        }
        return Optional.ofNullable(last);
    }

    private static Optional<Integer> findIntArg(Set<String> argNames, String... args) {
        return findArg(argNames, args).map(Integer::parseInt);
    }



    private Server startServer(String ip, int port) throws IOException {
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setHost(ip);
        connector.setPort(port);
        server.setConnectors(new ServerConnector[] { connector });

        ServletHandler handler = new ServletHandler();
        addMapping(handler, "/list", new ListConnectionsHandler(webSocketService));
        addMapping(handler, "/connect", new ConnectHandler(webSocketService));
        addMapping(handler, "/close", new CloseHandler(webSocketService));
        addMapping(handler, "/poll", new PollHandler(webSocketService));
        addMapping(handler, "/send", new SendHandler(webSocketService));
        addMapping(handler, "/*", GenericServlet.NOT_FOUND);
        server.setHandler(handler);
        try {
            server.start();
            return server;
        } catch (Exception e) {
            throw new IOException("Cannot start server on " + ip + ":" + port, e);
        }
    }

    private ServletHandler addMapping(ServletHandler handler, String path, Servlet servlet) {
        ServletHolder servletHolder = new ServletHolder();
        servletHolder.setServlet(servlet);
        handler.addServletWithMapping(servletHolder,
                (contextPath.endsWith("/") ? contextPath : contextPath + "/") + (path.startsWith("/") ? path.substring(1) : path));
        return handler;
    }

}
