package com.netikras.tools.wshttp;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Getter
@ToString
public class WSConn {
    private final String url;
    private final String id;

    private final Map<String, Consumer<byte[]>> messageListeners;
    private final Map<String, Consumer<Exception>> errorListeners;
    private final Map<String, BiConsumer<Integer, String>> closedListeners;

    private boolean open;
    private String closeReason;
    private Integer closeCode;
    private Boolean closedByRemote;

    private WebSocketClient ws;

    private final BlockingDeque<byte[]> pending;
    private boolean queuePending;

    public WSConn(String id, String url, BlockingDeque<byte[]> queue) {
        this.url = url;
        this.id = id;
        this.messageListeners = new ConcurrentHashMap<>();
        this.errorListeners = new ConcurrentHashMap<>();
        this.closedListeners = new ConcurrentHashMap<>();
        this.closeCode = Integer.MIN_VALUE;
        this.pending = queue;
        this.queuePending = true;
    }

    public WSConn(String id, String url, int queueSize) {
        this(id, url, new CircularDequeue<>(Math.max(queueSize, 1)));
        if (queueSize < 1) {
            queuePending = false;
        }
    }

    public WSConn(String id, String url) {
        this(id, url, 1);
        queuePending = false;
    }

    public WSConn(String url) {
        this(url, 20);
    }

    public WSConn(String url, int queueSize) {
        this(generateId(), url, queueSize);
    }

    public static String generateId() {
        return UUID.randomUUID().toString();
    }

    public String subscribeMessage(Consumer<byte[]> handler, Function<byte[], Boolean> until) {
        String subscriptionId = UUID.randomUUID().toString();
        if (handler != null) {
            Consumer<byte[]> listener = until == null ? handler : msg -> {
                if (until.apply(msg)) {
                    unsubscribe(subscriptionId);
                } else {
                    handler.accept(msg);
                }
            };
            messageListeners.put(subscriptionId, listener);

            try {
                while (queuePending && !getPending().isEmpty() && (until == null || !until.apply(getPending().peek()))) {
                    byte[] message = getPending().poll(1, TimeUnit.SECONDS);
                    listener.accept(message);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return subscriptionId;
    }

    public String subscribeErrors(Consumer<Exception> handler, Function<Exception, Boolean> until) {
        String subscriptionId = UUID.randomUUID().toString();
        if (handler != null) {
            errorListeners.put(subscriptionId, until == null ? handler : err -> {
                if (until.apply(err)) {
                    unsubscribe(subscriptionId);
                } else {
                    handler.accept(err);
                }
            });
        }
        return subscriptionId;
    }

    public String subscribeClose(BiConsumer<Integer, String> handler, BiFunction<Integer, String, Boolean> until) {
        String subscriptionId = UUID.randomUUID().toString();
        if (handler != null) {
            closedListeners.put(subscriptionId, until == null ? handler : (code, reason) -> {
                if (until.apply(code, reason)) {
                    unsubscribe(subscriptionId);
                } else {
                    handler.accept(code, reason);
                }
            });
        }
        return subscriptionId;
    }

    public void unsubscribe(String subId) {
        messageListeners.remove(subId);
        errorListeners.remove(subId);
        closedListeners.remove(subId);
    }

    public void setWs(WebSocketClient ws) throws IllegalStateException {
        if (this.ws != null) {
            throw new IllegalStateException("WS already set: " + ws);
        }
        this.ws = ws;
    }

    public boolean isUsable() {
        return open && !isClosed();
    }

    private void assertUsable() throws IOException {
        if (!isUsable()) {
            throw new IOException("Connection " + getId() + " is not usable");
        }
    }

    public boolean isClosed() {
        return closedByRemote != null || closeCode != Integer.MIN_VALUE || closeReason != null;
    }

    public synchronized void open() throws IllegalStateException {
        if (isOpen()) {
            throw new IllegalStateException("Already open " + getId());
        } else if (isClosed()) {
            throw new IllegalStateException("Has been closed " + getId());
        } else {
            open = true;
        }
    }

    public void send(String data) throws IOException {
        assertUsable();
        getWs().send(data);
    }

    public void send(byte[] data) throws IOException {
        assertUsable();
        getWs().send(data);
    }

    public void close(int code, String reason) throws IOException {
//        getWs().close(code, reason);
//        markClosed(code, reason, false);
        recvClose(code, reason, false);
    }

    public synchronized void markClosed(int code, String reason, boolean byRemote) throws IOException {
        if (isClosed()) {
            throw new IOException("Already closed " + getId());
        } else {
            this.closeCode = code;
            this.closeReason = reason;
            this.closedByRemote = byRemote;
        }
    }

    public void recvMessage(String msg) {
        if (msg != null) {
            recvMessage(msg.getBytes());
        }
    }

    public void recvMessage(byte[] msg) {
        if (msg != null) {
            List<Consumer<byte[]>> listeners = getMessageListeners().values().stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            if (listeners.isEmpty() && isQueuePending()) {
                getPending().offer(msg);
            } else {
                listeners.forEach(lsnr -> lsnr.accept(msg));
            }
        }
    }

    public void recvError(Exception e) {
        getErrorListeners().values().stream()
                .filter(Objects::nonNull)
                .forEach(lsnr -> lsnr.accept(e));
    }

    public void recvClose(int code, String msg, boolean remote) {
        try {
            markClosed(code, msg, remote);
        } catch (IOException e) {
            throw new RuntimeException("Cannot mark as closed " + getId(), e);
        }
        getClosedListeners().values().stream()
                .filter(Objects::nonNull)
                .forEach(lsnr -> lsnr.accept(code, msg));
    }

    public static class CircularDequeue<T> extends LinkedBlockingDeque<T> {

        public CircularDequeue() {
        }

        public CircularDequeue(int capacity) {
            super(capacity);
        }

        public CircularDequeue(Collection<? extends T> c) {
            super(c);
        }

        @Override
        public boolean offerLast(T t) {
            if (super.offerLast(t)) {
                return true;
            } else {
                super.removeFirst();
                return super.offerLast(t);
            }
        }
    }
}
